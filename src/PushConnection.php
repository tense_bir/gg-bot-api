<?php
/**
 * Biblioteka implementująca BotAPI GG <https://boty.gg.pl>
 * Copyright (C) 2013-2016 Xevin Consulting Limited Marcin Bagiński <marcin.baginski@firma.gg.pl>
 * Modified by KsaR 2016-2017 <https://github.com/KsaR99/>
 * Modified by Rodziu 2017 <mateusz.rohde@gmail.com>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>
 */

namespace GG;

/**
 * @brief Klasa reprezentująca połączenie PUSH z BotMasterem.
 * Autoryzuje połączenie w trakcie tworzenia i wysyła wiadomości do BotMastera.
 */
class PushConnection{
	const CURL_VERBOSE = false;
	/**
	 * Statusy GG
	 */
	const STATUS_AWAY = 'away';
	const STATUS_FFC = 'ffc';
	const STATUS_BACK = 'back';
	const STATUS_DND = 'dnd';
	const STATUS_INVISIBLE = 'invisible';
	/**
	 * Obiekt autoryzacji
	 *
	 * Typ BotAPIAuthorization
	 */
	public static $BOT_API_LOGIN = null;
	public static $BOT_API_PASSWORD = null;
	public static $lastGg;
	public static $lastAuthorization;
	private $authorization;
	private $gg;

	/**
	 * Konstruktor PushConnection - przeprowadza autoryzację
	 *
	 * @param int $botGGNumber numer GG bota
	 * @param string $userName login
	 * @param string $password hasło
	 */
	public function __construct($botGGNumber = null, $userName = null, $password = null){
		if(empty(self::$lastAuthorization) || !self::$lastAuthorization->isAuthorized()
			|| ($botGGNumber !== self::$lastGg && $botGGNumber !== null)){
			if($userName === null && self::$BOT_API_LOGIN !== null){
				$userName = self::$BOT_API_LOGIN;
			}
			if($password === null && self::$BOT_API_PASSWORD !== null){
				$password = self::$BOT_API_PASSWORD;
			}
			self::$lastGg = $botGGNumber;
			self::$lastAuthorization = new BotAPIAuthorization(self::$lastGg, $userName, $password);
		}
		$this->gg = self::$lastGg;
		$this->authorization = self::$lastAuthorization;
	}

	/**
	 * Wysyła wiadomość (obiekt lub tablicę obiektów MessageBuilder) do BotMastera.
	 *
	 * @param $messages
	 *
	 * @return bool
	 * @throws UnableToSendMessageException
	 * @internal param array|MessageBuilder $message obiekt lub tablica obiektów MessageBuilder
	 *
	 */
	public function push($messages){
		if(!$this->authorization->isAuthorized()){
			return false;
		}
		if(!is_array($messages)){
			$messages = [$messages];
		}
		$data = $this->authorization->getServerAndToken();
		foreach($messages as $message){
			$ch = $this->getSingleCurlHandle();
			curl_setopt_array($ch, [
				CURLOPT_URL        => 'https://'.$data['server'].'/sendMessage/'.$this->gg,
				CURLOPT_POSTFIELDS => 'msg='.urlencode($message->getProtocolMessage()).'&to='.implode(',', $message->recipientNumbers),
				CURLOPT_HEADER     => false,
				CURLOPT_HTTPHEADER => [
					'BotApi-Version: '.MessageBuilder::BOT_API_VERSION,
					'Token: '.$data['token']
				]]);
			$r = curl_exec($ch);
			curl_close($ch);
			if(strpos($r, /** @lang text */
					'<status>0</status>') === false){
				throw new UnableToSendMessageException('Nie udało się wysłać wiadomości.');
			}
		}
		return true;
	}

	/**
	 * Tworzy i zwraca uchwyt do nowego żądania cURL
	 *
	 * @return resource cURL handle
	 */
	private function getSingleCurlHandle(){
		$ch = curl_init();
		curl_setopt_array($ch, [
			CURLOPT_SSL_VERIFYHOST => false,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FAILONERROR    => true,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_TIMEOUT        => 5,
			CURLOPT_CONNECTTIMEOUT => 5,
			CURLOPT_BINARYTRANSFER => true,
			CURLOPT_POST           => true,
			CURLOPT_HEADER         => true,
			CURLOPT_VERBOSE        => self::CURL_VERBOSE
		]);
		return $ch;
	}

	/**
	 * Ustawia opis botowi.
	 *
	 * @param string $desc Treść opisu
	 * @param string $status Typ opisu
	 *
	 * @throws UnableToSetStatusException
	 */
	public function setStatus($desc, $status = ''){
		if(!$this->authorization->isAuthorized()){
			return;
		}
		switch($status){
			case self::STATUS_AWAY:
				$h = empty($desc) ? 3 : 5;
				break;
			case self::STATUS_FFC:
				$h = empty($desc) ? 23 : 24;
				break;
			case self::STATUS_BACK:
				$h = empty($desc) ? 2 : 4;
				break;
			case self::STATUS_DND:
				$h = empty($desc) ? 33 : 34;
				break;
			case self::STATUS_INVISIBLE:
				$h = empty($desc) ? 20 : 22;
				break;
			default:
				$h = 0;
		}
		$data = $this->authorization->getServerAndToken();
		$ch = $this->getSingleCurlHandle();
		curl_setopt_array($ch, [
			CURLOPT_URL        => 'https://'.$data['server'].'/setStatus/'.$this->gg,
			CURLOPT_POSTFIELDS => 'status='.$h.'&desc='.urlencode($desc),
			CURLOPT_HEADER     => false,
			CURLOPT_HTTPHEADER => [
				'BotApi-Version: '.MessageBuilder::BOT_API_VERSION,
				'Token: '.$data['token']
			]
		]);
		$r = curl_exec($ch);
		curl_close($ch);
		if(strpos($r, /** @lang text */
				'<status>0</status>') === false){
			throw new UnableToSetStatusException('Niepowodzenie ustawiania statusu.');
		}
	}

	/**
	 * Wysyła obrazek do Bot mastera
	 *
	 * @param $data
	 *
	 * @return bool
	 */
	public function putImage($data){
		return ($this->authorization->isAuthorized()) ?
			strpos($this->imageCurl('put', $data), /** @lang text */
				'<status>0</status>') !== false :
			false;
	}

	/**
	 * Tworzy i zwraca uchwyt do nowego żądania cURL
	 *
	 * @param $type
	 * @param $post
	 *
	 * @return mixed
	 */
	private function imageCurl($type, $post){
		$ch = $this->getSingleCurlHandle();
		curl_setopt_array($ch, [
			CURLOPT_URL        => "https://botapi.gadu-gadu.pl/botmaster/{$type}Image/{$this->gg}",
			CURLOPT_POSTFIELDS => $post,
			CURLOPT_HTTPHEADER => [
				'BotApi-Version: '.MessageBuilder::BOT_API_VERSION,
				'Token: '.$this->authorization->getServerAndToken()['token'],
				'Expect: '
			]
		]);
		$r = curl_exec($ch);
		curl_close($ch);
		return $r;
	}

	/**
	 * Pobiera obrazek z Bot mastera
	 *
	 * @param $hash
	 *
	 * @return bool
	 */
	public function getImage($hash){
		return ($this->authorization->isAuthorized()) ?
			explode("\r\n\r\n", $this->imageCurl('get', 'hash='.$hash), 2)[1] :
			false;
	}

	/**
	 * Sprawdza czy Botmaster ma obrazek
	 *
	 * @param $hash
	 *
	 * @return bool
	 */
	public function existsImage($hash){
		return ($this->authorization->isAuthorized()) ?
			strpos($this->imageCurl('exists', 'hash='.$hash), /** @lang text */
				'<status>0</status>') !== false :
			false;
	}

	/**
	 * Sprawdza, czy numer jest botem
	 *
	 * @param $ggId
	 *
	 * @return bool
	 */
	public function isBot($ggId){
		if(!$this->authorization->isAuthorized()){
			return false;
		}
		$ch = $this->getSingleCurlHandle();
		curl_setopt_array($ch, [
			CURLOPT_URL        => 'https://botapi.gadu-gadu.pl/botmaster/isBot/'.$this->gg,
			CURLOPT_POSTFIELDS => 'check_ggid='.$ggId,
			CURLOPT_HEADER     => false,
			CURLOPT_HTTPHEADER => [
				'BotApi-Version: '.MessageBuilder::BOT_API_VERSION,
				'Token: '.$this->authorization->getServerAndToken()['token']
			]
		]);
		$r = curl_exec($ch);
		curl_close($ch);
		return strpos($r, /** @lang text */
				'<status>1</status>') !== false;
	}
}