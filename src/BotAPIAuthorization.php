<?php
/**
 * Biblioteka implementująca BotAPI GG <https://boty.gg.pl>
 * Copyright (C) 2013-2016 Xevin Consulting Limited Marcin Bagiński <marcin.baginski@firma.gg.pl>
 * Modified by KsaR 2016-2017 <https://github.com/KsaR99/>
 * Modified by Rodziu 2017 <mateusz.rohde@gmail.com>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>
 */

namespace GG;

/**
 * Pomocnicza klasa do autoryzacji przez HTTP
 */
class BotAPIAuthorization{
	private $data = [
		'token'  => null,
		'server' => null,
		'port'   => null
	];
	private $isValid;

	/**
	 * @return bool true jeśli autoryzacja przebiegła prawidłowo
	 */
	public function isAuthorized(){
		return $this->isValid;
	}

	public function __construct($ggId, $userName, $password){
		$this->isValid = $this->getData($ggId, $userName, $password);
	}

	private function getData($ggId, $userName, $password){
		$ch = curl_init();
		curl_setopt_array($ch, [
			CURLOPT_URL            => 'https://botapi.gadu-gadu.pl/botmaster/getToken/'.$ggId,
			CURLOPT_USERPWD        => $userName.':'.$password,
			CURLOPT_HTTPAUTH       => CURLAUTH_BASIC,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_VERBOSE        => PushConnection::CURL_VERBOSE,
			CURLOPT_HTTPHEADER     => ['BotApi-Version: '.MessageBuilder::BOT_API_VERSION],
		]);
		$xml = curl_exec($ch);
		curl_close($ch);
		if(!preg_match(/** @lang text */
			'~<token>(.+?)</token><server>(.+?)</server><port>(\d+)</port>~', $xml, $out
		)){
			return false;
		}
		$this->data = [
			'token'  => $out[1],
			'server' => $out[2],
			'port'   => $out[3]
		];
		return true;
	}

	/**
	 * Pobiera aktywny token, port i adres BotMastera
	 *
	 * @return array
	 */
	public function getServerAndToken(){
		return $this->data;
	}
}